package com.dojo.questoes.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dojo.questoes.model.Questao;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface QuestaoDaoRepository extends CrudRepository<Questao, Long>, JpaSpecificationExecutor<Questao> {

	/*@Query(" select e.id from Endereco e where e.pessoa.id = :idPessoa and e.cep = :cep "+
		   " and e.numero = :numero and e.cidade = :cidade and e.uf = :uf ")
	Long verificarExistencia(@Param("idPessoa") Long idPessoa, @Param("cep") Integer cep, @Param("numero") String numero, 
			                 @Param("cidade") String cidade, @Param("uf") String uf);*/
}
