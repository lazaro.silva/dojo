package com.dojo.questoes.helper;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RestResultDto<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private T data;
	private Boolean success;
	private List<String> erros;
	
	public RestResultDto() {
		super();
	}
	
	public static ObjectMapper getMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
	    objectMapper.configure( SerializationFeature.INDENT_OUTPUT, true );
	    objectMapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    objectMapper.configure( SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	    objectMapper.configure( Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
	    objectMapper.configure( DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
	    
	    objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
	    
	    /*objectMapper.configure( DeserializationFeature.UNWRAP_ROOT_VALUE, true);
	    objectMapper.configure( DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS, true);
	    objectMapper.configure( DeserializationFeature.WRAP_EXCEPTIONS, false);*/
	    
	    objectMapper.setVisibility( PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY );
	    objectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
	    
	    //objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
	    //objectMapper.enableDefaultTyping( ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY );
	    
	    return objectMapper;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public List<String> getErros() {
		return erros;
	}

	public void setErros(List<String> erros) {
		this.erros = erros;
	}

	public T getValue(TypeReference<T> typeRef) {
		if(this.getData() != null && !this.getData().toString().equals("")) {
			return getMapper().convertValue(this.getData(), typeRef);
		}
		return null;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@JsonIgnore
	public static Map<String, Object> getMap(String json) {
		Map<String, Object> ret = null;
		try {
			ret = RestResultDto.getMapper().readValue(json, HashMap.class);
		}
		catch(IOException ioe) {
			ret = RestResultDto.getMapper().convertValue(json, new TypeReference<HashMap>() {});
		}
		return ret;
	}
	
	
	@JsonIgnore
	public static String getStrJson(Object value) {
		if(value == null) {
			return null;
		}
		
		try {
			return getMapper().writeValueAsString(value);
		}
		catch(IOException ioe) {
			throw new RuntimeException(ioe.getMessage()); 
		}
	}
}
