package com.dojo.questoes.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.dojo.questoes.model.OpcoesQuestao;
import com.dojo.questoes.model.Questao;
import com.dojo.questoes.model.Questionario;
import com.dojo.questoes.model.QuestionarioQuestao;
import com.dojo.questoes.repository.OpcoesQuestaoDaoRepository;
import com.dojo.questoes.repository.QuestaoDaoRepository;
import com.dojo.questoes.repository.QuestionarioDaoRepository;
import com.dojo.questoes.repository.QuestionarioQuestaoDaoRepository;

@Component("questionarioDaoIf")
public class QuestionarioDaoImpl {
	
	@Autowired
	private QuestaoDaoRepository questaoDaoRepository;
	@Autowired
	private OpcoesQuestaoDaoRepository opcoesQuestaoDaoRepository;
	
	public QuestionarioDaoImpl() {
		super();
	}
	
	//@SuppressWarnings("unchecked")
	@Transactional(isolation=Isolation.READ_COMMITTED, propagation=Propagation.MANDATORY, rollbackFor=Exception.class)
	public Questao salvarQuestao(Questao entity) {
		
		List<OpcoesQuestao> opcoes = null;
		if(entity.getOpcoes() != null && !entity.getOpcoes().isEmpty()) {
			opcoes = new ArrayList<OpcoesQuestao>(entity.getOpcoes());
			entity.setOpcoes(null);
		}
		entity = questaoDaoRepository.save(entity);
		if(opcoes != null) {
			entity.setOpcoes(new ArrayList<OpcoesQuestao>(1));
			for(OpcoesQuestao o : opcoes) {
				o.setQuestao(entity);
				entity.getOpcoes().add(opcoesQuestaoDaoRepository.save(o));
			}
		}
		
		return entity;
	}
}