package com.dojo.questoes.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import com.dojo.questoes.model.OpcoesQuestao;


@Repository
public interface OpcoesQuestaoDaoRepository extends CrudRepository<OpcoesQuestao, Long>, JpaSpecificationExecutor<OpcoesQuestao> {

}
