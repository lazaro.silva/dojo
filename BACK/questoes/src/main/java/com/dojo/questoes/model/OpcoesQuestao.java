package com.dojo.questoes.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@javax.persistence.Entity
@Table(name="opcoes_questao")
public class OpcoesQuestao implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 983936297716356186L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "id_questao", nullable=false)
	private Questao questao;
	
	@Column(name = "opcao_questao", nullable=false, length=1000)
	private String opcaoQuestao;
	
	public OpcoesQuestao() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Questao getQuestao() {
		return questao;
	}

	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	public String getOpcaoQuestao() {
		return opcaoQuestao;
	}

	public void setOpcaoQuestao(String opcaoQuestao) {
		this.opcaoQuestao = opcaoQuestao;
	}
	
}
