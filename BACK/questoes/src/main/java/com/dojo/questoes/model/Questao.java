package com.dojo.questoes.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.dojo.questoes.helper.TipoRespostaEnum;
import com.dojo.questoes.helper.TipoRespostaEnumConverter;

@javax.persistence.Entity
@Table(name="questao")
public class Questao implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2210603665996817428L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	
	@Column(nullable=false, length=1000)
	private String questao;
	
	@Convert(converter = TipoRespostaEnumConverter.class)
	@Column(name = "tipo_resposta", nullable=false)
	private TipoRespostaEnum tipoResposta;
	
	@OneToMany(mappedBy="questao", fetch = FetchType.EAGER, cascade=CascadeType.REMOVE)
	private List<OpcoesQuestao> opcoes;
	
	public Questao() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuestao() {
		return questao;
	}

	public void setQuestao(String questao) {
		this.questao = questao;
	}

	public TipoRespostaEnum getTipoResposta() {
		return tipoResposta;
	}

	public void setTipoResposta(TipoRespostaEnum tipoResposta) {
		this.tipoResposta = tipoResposta;
	}

	public List<OpcoesQuestao> getOpcoes() {
		return opcoes;
	}

	public void setOpcoes(List<OpcoesQuestao> opcoes) {
		this.opcoes = opcoes;
	}

}
