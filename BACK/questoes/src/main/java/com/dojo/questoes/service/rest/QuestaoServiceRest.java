package com.dojo.questoes.service.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dojo.questoes.dao.QuestionarioDaoImpl;
import com.dojo.questoes.helper.RestResultDto;
import com.dojo.questoes.helper.TipoRespostaEnum;
import com.dojo.questoes.model.OpcoesQuestao;
import com.dojo.questoes.model.Questao;

@RestController
@RequestMapping("/questao")
@Service
public class QuestaoServiceRest {
	
	@Autowired
	private QuestionarioDaoImpl questionarioDaoIf;

	public QuestaoServiceRest() {
		super();
	}
	
	private RestResultDto<Questao> validarQuestao(Questao record) {
		RestResultDto<Questao> result = new RestResultDto<Questao>();
		List<String> erros = new ArrayList<String>(1);
		if(record == null) {
			erros.add("Registro não foi informado!");
		}
		else {
			if(record.getQuestao() == null || record.getQuestao().isEmpty()) {
				erros.add("A Questão não foi informada!");
			}
			else if(record.getQuestao().length() > 1000) {
				erros.add("Tamanho do campo Questão não pode ser maior que 1000 caracteres!");
			}
			
			if(record.getTipoResposta() == null) {
				erros.add("Tipo de Resposta do questionário não informado!");
			}
			else {
				if((TipoRespostaEnum.ABERTO.equals(record.getTipoResposta()) || TipoRespostaEnum.BOOLEANO.equals(record.getTipoResposta())) 
						&& (record.getOpcoes() != null && !record.getOpcoes().isEmpty())) {
					erros.add("Se o Tipo de Resposta do questionário é aberta / booleana, não se pode informar opções de questões!");
				}
			}
			
			if(record.getOpcoes() != null && !record.getOpcoes().isEmpty()) {
				for(OpcoesQuestao oq : record.getOpcoes()) {
					if(oq.getOpcaoQuestao() == null || oq.getOpcaoQuestao().isEmpty()) {
						erros.add("A Opção de Questão não foi informada!");
					}
					else if(oq.getOpcaoQuestao().length() > 1000) {
						erros.add("Tamanho do campo Opção da Questão não pode ser maior que 1000 caracteres!");
					}
				}
			}
		}
		
		if(!erros.isEmpty()) {
			result.setErros(erros);
			result.setSuccess(false);
		}
		else {
			result.setSuccess(true);
		}
		
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @Transactional(isolation=Isolation.READ_COMMITTED, propagation=Propagation.REQUIRES_NEW, rollbackFor=Exception.class)
    public RestResultDto<Questao> save(@RequestBody Questao record) {
		
		RestResultDto<Questao> result = validarQuestao(record);
		if(!result.getSuccess()) {
			return result;
		}
		record = questionarioDaoIf.salvarQuestao(record);
		result.setData(record);
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public RestResultDto<List<Questao>> obterTodos() {
		
		RestResultDto<List<Questao>> result = new RestResultDto<List<Questao>>();
		result.setData(questionarioDaoIf.obterTodasQuestoes());
		result.setSuccess(true);
		return result;
	}
}
