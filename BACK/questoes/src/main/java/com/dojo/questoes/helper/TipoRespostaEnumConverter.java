package com.dojo.questoes.helper;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class TipoRespostaEnumConverter implements AttributeConverter<TipoRespostaEnum, Integer> {

	@Override
	public Integer convertToDatabaseColumn(TipoRespostaEnum arg0) {
		// TODO Auto-generated method stub
		if(arg0 != null) {
			return arg0.getId();
		}
		return null;
	}

	@Override
	public TipoRespostaEnum convertToEntityAttribute(Integer arg0) {
		// TODO Auto-generated method stub
		if(arg0 != null) {
			return TipoRespostaEnum.findByCode(arg0);
		}
		return null;
	}

	
}
