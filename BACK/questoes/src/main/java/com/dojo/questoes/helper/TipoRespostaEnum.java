package com.dojo.questoes.helper;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = TipoRespostaEnumDeserializer.class)
public enum TipoRespostaEnum {
	
	BOOLEANO(1, "Sim/Nao"),
	MULTIPLA_ESCOLHA(2, "Múltipla escolha"),
	ABERTO(3, "Aberto"),
	COMPOSICAO(4, "Composição de Questões Booleanas"),
	;
	
	private Integer codigo;
	private String descricao;
	
	private TipoRespostaEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public static TipoRespostaEnum findByCode(Integer code) {
		
		if(code == null) {
			return null;
		}
		
		for (TipoRespostaEnum enum1 : TipoRespostaEnum.values()) {
			if (enum1.getId().equals(code)) {
				return enum1;
			}
		}
		throw new IllegalArgumentException("Invalid Code.");
	}
	
	public String toString() {
		return descricao;
	}

}
