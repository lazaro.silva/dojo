import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Questao } from '../model/questao.model';
import { environment } from '../../environments/environment';
import { Questionario } from '../model/questionario.model';
import { map } from 'rxjs/operators';
import { RestResult } from '../model/restresult.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionarioService {

  constructor(private http: HttpClient) { }

  obterQuestoes() {
    return this.http.get<RestResult>(`${environment.baseApiUrl}/questao`);
  }

  salvarQuestionario(record: Questionario) {
    return this.http.post<RestResult>(`${environment.baseApiUrl}/questionario`, {record});
  }

  salvarQuestao(record: Questao) {
    return this.http.post<RestResult>(`${environment.baseApiUrl}/questao`, {record});
  }
}
