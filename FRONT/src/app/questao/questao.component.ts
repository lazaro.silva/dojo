import { Component, OnInit } from '@angular/core';
import { Questao } from '../model/questao.model';
import { Enum } from '../model/enum.model';
import { OpcoesQuestao } from '../model/opcoesquestao.model';
import { QuestionarioService } from '../service/questionario.service';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Data } from '../provider/data.provider';

@Component({
  selector: 'app-questao',
  templateUrl: './questao.component.html',
  styleUrls: ['./questao.component.scss']
})
export class QuestaoComponent implements OnInit {

  questao: Questao;
  returnUrl: string;

  constructor(private service: QuestionarioService,
              private route: ActivatedRoute,
              private router: Router, private data: Data) { 
  }

  ngOnInit() {
    this.questao.tipoResposta = <Enum>{};
    this.questao.opcoes = [];
    let o = <OpcoesQuestao>{};
    o.opcaoQuestao = "";
    this.questao.opcoes.push(o);

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  novaOpcao() {
    let o = <OpcoesQuestao>{};
    o.opcaoQuestao = "";
    this.questao.opcoes.push(o);
  }

  salvar() {
    this.service.salvarQuestao(this.questao).pipe(first()).subscribe(result => { 
      if(result.success) {
        //RETORNAR A PAGINA ANTERIOR
        this.data.storage = this.questao;
        this.router.navigate([this.returnUrl]);
      }
      else {
        let erro = '';
        result.erros.forEach(e => {
          erro+=(e+'\n');
        });
        alert(erro);
      }
    });
  }

  cancelar() {
    this.data.storage = null;
    this.router.navigate([this.returnUrl]);
  }

}
