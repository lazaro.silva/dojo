import { Enum } from './enum.model';
import { OpcoesQuestao } from './opcoesquestao.model';

export interface Questao {
    id: number;
    questao: string;
    tipoResposta: Enum
    opcoes: OpcoesQuestao[];
}