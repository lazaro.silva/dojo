import { Questao } from './questao.model';

export interface QuestionarioQuestao {
    id: number;
    questao: Questao;
}