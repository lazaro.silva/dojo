import { Questao } from './questao.model';
import { QuestionarioQuestao } from './questionarioquestao.model';

export interface Questionario {
    id: number;
    questoes: QuestionarioQuestao[];
    nome: string;
}