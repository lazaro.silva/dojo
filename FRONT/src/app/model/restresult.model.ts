export interface RestResult {
    data: any;
    success: boolean;
    erros: string[];
}