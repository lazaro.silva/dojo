import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { QuestionarioComponent } from './questionario/questionario.component';
import { QuestaoComponent } from './questao/questao.component';


const routes: Routes = [
  {
    path: '',
    component: AppComponent
  },
  { 
    path: 'questionario', 
    component: QuestionarioComponent 
  },
  { 
    path: 'questao', 
    component: QuestaoComponent 
  },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
