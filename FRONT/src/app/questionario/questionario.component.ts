import { Component, OnInit } from '@angular/core';
import { Questionario } from '../model/questionario.model';
import { Questao } from '../model/questao.model';
import { QuestionarioService } from '../service/questionario.service';
import { first } from 'rxjs/operators';
import { Data } from '../provider/data.provider';

@Component({
  selector: 'app-questionario',
  templateUrl: './questionario.component.html',
  styleUrls: ['./questionario.component.scss']
})
export class QuestionarioComponent implements OnInit {

  questionario: Questionario;
  questoes: Questao[];
  ids: number[];

  constructor(private service: QuestionarioService, private data: Data) { 
    this.questionario = <Questionario>{};
    
  }

  ngOnInit() {
    console.log('ngOnInit');
    this.service.obterQuestoes().pipe(first()).subscribe(result => { 
      if(result.success) {
        this.questoes = result.data;
      }
      else {
        let erro = '';
        result.erros.forEach(e => {
          erro+=(e+'\n');
        });
        alert(erro);
      }
    });
  }

  changeCheck(id, event) {
    if (event.value)
      this.ids.push(parseInt(id, 10));
    else {
      let idN = parseInt(id, 10);
      let idx = this.ids.indexOf(idN);
      if (idx > -1)
        this.ids.splice(idx, 1);
    }
  }

  salvar() {

  }

  cancelar() {
    
  }

  novaQuestao() {

  }

}
